package be.iesca.controleur;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;
import be.iesca.usecaseimpl.GestionUsersImpl;
/**
 * Contr�leur de l'application (couche logique)
 * C'est un singleton.
 * @author Olivier Legrand
 *
 */
public class GestionnaireUseCases implements GestionUsers {

	private static final GestionnaireUseCases INSTANCE = new GestionnaireUseCases();
	
	private User user; // null si pas identifi� par le syst�me
	private GestionUsersImpl gestionUsers;

	public static GestionnaireUseCases getInstance() {
		return INSTANCE;
	}

	private GestionnaireUseCases() {
		this.gestionUsers = new GestionUsersImpl();
		this.user = null; // pas de user connect�
	}

	@Override
	public void connecterUser(Bundle bundle) 
	{
		if (user == null) 
		{
			this.gestionUsers.connecterUser(bundle);
			if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) 
			{
				this.user = (User) bundle.get(Bundle.USER);
			}
		}
		else 
		{ // un utilisateur est d�j� connect�
			bundle.put(Bundle.MESSAGE,
					"Op�ration impossible. Un utilisateur est d�j� connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		}
	}

	public void deconnecterUser(Bundle bundle) 
	{
		if (user == null) 
		{ // pas de user identifi�
			bundle.put(Bundle.MESSAGE,
					"Op�ration impossible. Pas d'utilisateur connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		}
		else 
		{
			this.user = null; // utilisateur d�connect�
			bundle.put(Bundle.MESSAGE, "Vous avez �t� d�connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, true);
		}
	}
	
	public void inscrireUser(Bundle bundle){
		this.gestionUsers.inscrireUser(bundle);
		
	}
	
public void modifierUser(Bundle bundle) {
		
		if (user == null) { // pas de user identifi�
			bundle.put(Bundle.MESSAGE,
					"Op�ration impossible. Pas d'utilisateur connect�.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		} else {
			this.gestionUsers.modifierUser(bundle);
		}
	}


}
