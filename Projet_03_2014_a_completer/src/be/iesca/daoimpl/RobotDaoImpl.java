package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import be.iesca.dao.RobotDao;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;

public class RobotDaoImpl implements RobotDao {
	
	private static final String VIDER = "delete from robots where pseudo_user = ?";
	private static final String AJOUT = "insert into robots (nom,code,pseudo_user) values (?,?,?)";
	private static final String LISTER = "select nom, code from robots where pseudo_user = ?";
	private static final String SUPPRIMER = "delete from robots where pseudo_user = ? AND nom = ?";
	
	
	
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}


	@Override
	public boolean supprimerRobot(User user, ClasseRobot robot) {
		boolean suppressionReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPPRIMER);
			ps.setString(1, user.getNom().trim());
			ps.setString(2, robot.getNom().trim());
			int nb = ps.executeUpdate();
			if (nb == 1) suppressionReussie = true;
				
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return suppressionReussie;
	}

	@Override
	public List<ClasseRobot> listerRobot(User user) {
		List<ClasseRobot> liste = new ArrayList<ClasseRobot>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTER);
			ps.setString(1, user.getNom().trim());
			rs= ps.executeQuery();
			String nom = "";
			while(rs.next())
			{
				nom = rs.getString("nom");
				ClasseRobot robot = new ClasseRobot(nom, rs.getString(2));
				liste.add(robot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return liste;
	}

	@Override
	public boolean vider(User user) {
		
		boolean vider = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(VIDER);
			ps.setString(1, user.getNom().trim());
			
			int resultat = ps.executeUpdate();
			System.out.println(resultat);
			if (resultat > 0) {
				vider = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return vider;
	}

	@Override
	public boolean ajouterRobot(ClasseRobot robot, User user) {
		
		
		boolean ajout = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUT);
			ps.setString(1, robot.getNom().trim());
			ps.setString(2, user.getNom().trim());    //a modifier pour le code du robot
			ps.setString(3, user.getNom().trim());
			
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajout = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return ajout;
	}

}
