// demoulin S�bastien
package be.iesca.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;
import be.iesca.usecaseimpl.GestionUsersImpl;

public class TestNG_Fonctionnels {
	
	protected GestionUsers gestion;
	private Bundle bundle=new Bundle();
	private User users;
	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiserLeDao() {
		gestion = new GestionUsersImpl();
	}
	@BeforeClass
	// sera ex�cut� avant toutes les m�thodes
	public void initialiserListeUsers() {
		users = new User("test@gmail.com", "testeur", "passtesteur");
		bundle.put(Bundle.USER, users);
	}
	@Test
	public void testInscription()
	{
		
		
		gestion.inscrireUser(bundle);
		assertTrue((boolean)bundle.get(Bundle.OPERATION_REUSSIE));
			
		
	}
	@Test(dependsOnMethods = { "testInscription"})
	public void testModifier()
	{
		User user= (User) bundle.get(Bundle.USER);
		user.setEmail(user.getEmail()+"!");
		user.setNom(user.getNom()+"!");
		user.setPassword(user.getPassword()+"!");
		gestion.modifierUser(bundle);
		assertEquals(bundle.get(Bundle.USER),user);	
	}

}
