package be.iesca.tests;
import static org.testng.AssertJUnit.*;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.daoimpl.RobotDaoImpl;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;

public class TestNG_RobotsDaoImpl {
	
	private User user = new User();
	private ClasseRobot robot1 = new ClasseRobot();
	private ClasseRobot robot2 = new ClasseRobot();
	private List<ClasseRobot> robots;
	private RobotDaoImpl dao = new RobotDaoImpl();
	
	@BeforeClass
	public void initialiserListeRobot()
	{
		robots = new ArrayList<ClasseRobot>();
		
		robot1.setNom("Crazy");
		robot2.setNom("Corner");
		robots.add(robot1);
		robots.add(robot2);
		
		user.setEmail("test@test.com");
		user.setPassword("test");
		user.setNom("toto");
		
		
		
	}
  @Test
  public void AjoutTest() {
	  //donn�es factices pour le test;  -> 2 robots fictifs pour 1 user toto
	  for(ClasseRobot r : robots)
	  {
		  assertTrue(dao.ajouterRobot(r, user));
		 
	  }
	  
  }
  
  @Test (dependsOnMethods = { "AjoutTest" })
  public void listerTest()
  {
	  List<ClasseRobot>robotsObtenus = dao.listerRobot(user);
	  for (int i = 0; i < robots.size(); i++) {
			assertEquals(robotsObtenus.get(i), robots.get(i));}
	  
  }
  
  @Test (dependsOnMethods = { "listerTest" })
  public void supprimerTest()
  {
	  for (ClasseRobot r : robots) {
			assertTrue(dao.supprimerRobot(user, r));
		}
		List<ClasseRobot> robotsObtenus = dao.listerRobot(user);
		assertNotNull(robotsObtenus);
		assertEquals(0, robotsObtenus.size());
		
		
  }
  

  @Test (dependsOnMethods = { "supprimerTest" })
  public void viderTest(){
	  //vidage des robots de toto;

		robots.add(robot1);
		
		robots.add(robot2);
		
		
		 for(ClasseRobot r : robots)
		  {
			 dao.ajouterRobot(r, user);
			 
		  }
		 
	  assertTrue(dao.vider(user));
	  
  }
 
  
}
