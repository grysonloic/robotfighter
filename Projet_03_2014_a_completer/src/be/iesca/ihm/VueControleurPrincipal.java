package be.iesca.ihm;
/**
 *  Vue - Contr�leur principal
 */
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;

@SuppressWarnings("serial")
public class VueControleurPrincipal extends JPanel implements ChangeListener {
	
	private JLabel JlTitre = new JLabel("Accueil", JLabel.CENTER);
	
	private JButton jbConnecter = new JButton("Connecter");
	
	private JButton jbModifier = new JButton("Compte");
	private JButton jbMBataille = new JButton("Bataille");
	private JButton jbMRobot = new JButton("Robot");
	
	private Bundle bundle = new Bundle();
	private boolean connecte = false;
	private Model model;
	private GestionnaireUseCases gestionnaire;
	private JFrame jframe;	

	public VueControleurPrincipal(Model model, JFrame jframe) {
		this.gestionnaire=GestionnaireUseCases.getInstance();
		this.jframe=jframe;
		
		// bulle + clavier bouttons
		this.jbMBataille.setToolTipText("Acceder au menu des batailles (ALT+B)");
		this.jbMBataille.setMnemonic(KeyEvent.VK_B);
		
		this.jbModifier.setToolTipText("Modifier votre compte (ALT+C)");
		this.jbModifier.setMnemonic(KeyEvent.VK_C);
		
		this.jbMRobot.setToolTipText("Acceder au menu des robots (ALT+R)");
		this.jbMRobot.setMnemonic(KeyEvent.VK_R);
		
		this.jbConnecter.setToolTipText("Connectez-vous (ALT+C)");
		this.jbConnecter.setMnemonic(KeyEvent.VK_C);
		
		
		// ajouts des panels
		this.setLayout(new BorderLayout());
		this.add(creerPanelBoutons(), BorderLayout.CENTER);
		ajouterListenersBoutons();
		activerBoutons(false);

		if (model != null) {
			this.model = model;
			this.model.addChangeListener(this);
			majAffichage();
		}
	}

	protected void connecterDeconnecter() {
		if (this.connecte) 
		{ // d�connexion
			this.gestionnaire.deconnecterUser(bundle);
			if  ( (boolean) bundle.get(Bundle.OPERATION_REUSSIE) )
			{
				this.jbConnecter.setText("Connecter");
				this.jbConnecter.setToolTipText("Connectez-vous (ALT+C)");
				this.jbConnecter.setMnemonic(KeyEvent.VK_C);
				this.connecte=false;
				activerBoutons(false);
			}
		} 
		else 
		{ // connecte
			FenetreConnexion fc = new FenetreConnexion(this.jframe);
			fc.setVisible(true);
			fc.setModal(false);
			this.bundle= fc.getBundle();
			if  ( (boolean) bundle.get(Bundle.OPERATION_REUSSIE) ) 
			{
				this.connecte=true;
				activerBoutons(true);
				this.jbConnecter.setText("D�connecter");
				this.jbConnecter.setToolTipText("Deconnectez-vous (ALT+D)");
				this.jbConnecter.setMnemonic(KeyEvent.VK_D);
			}
		}
		this.model.setBundle(bundle);
	}

	protected void modifierCompte()
	{
		FenetreModification fm = new FenetreModification(this.jframe, bundle);
		fm.setVisible(true);
		if((boolean)fm.getBundle().get(Bundle.OPERATION_REUSSIE))
		{
			this.bundle=fm.getBundle();
		}
		this.model.setBundle(bundle);
	}
	
	private void menuBataille() {
		// TODO Auto-generated method stub
		
	}
	
	private void menuRobot() {
		MenuRobot MR = new MenuRobot(this);
		
	}

	private void activerBoutons(boolean b) {
		this.removeAll();
		if (b)	this.add(creerPanelBoutonsAcceuil(), BorderLayout.CENTER);
		else 	this.add(creerPanelBoutons(), BorderLayout.CENTER);
		
		this.revalidate();
		this.jframe.pack();
	}

	private JPanel creerPanelBoutons() {
		JPanel jpBoutons = new JPanel();
		jpBoutons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpBoutons.setBorder(new TitledBorder(""));
		jpBoutons.add(jbConnecter);
		return jpBoutons;
	}

	private JPanel creerPanelBoutonsAcceuil()
	{
		this.JlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(JlTitre, BorderLayout.NORTH);
		
		JPanel jpBoutons = new JPanel();
		
		jpBoutons.setLayout(new GridLayout(2, 2, 70, 30));
		
		jpBoutons.add(jbModifier);
		jpBoutons.add(jbMBataille);
		jpBoutons.add(jbMRobot);
		jpBoutons.add(jbConnecter);
		
		return jpBoutons;
	}
	
	private void ajouterListenersBoutons() {
		// listeners boutons
		jbConnecter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				connecterDeconnecter();
			}
		});
		
		jbModifier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				modifierCompte();
			}
		});
	
		jbMBataille.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuBataille();
			}

		});
		
		jbMRobot.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				menuRobot();
			}

		});
	}

	private void majAffichage() {
		if (model == null)
			return;
		// � compl�ter
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		majAffichage();
	}
	
	public Bundle getBundle()
	{
		return this.bundle;
	}
	
	public Model getModel()
	{
		return this.model;
	}

}
