package be.iesca.ihm;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class ListerRobot extends JPanel{

	private static final long serialVersionUID = 1L;
	private JLabel JlRobot = new JLabel ("Vous n'avez pas de robots", JLabel.CENTER);
	
	public ListerRobot()
	{
		this.setBorder(new TitledBorder("Liste Robots"));
		this.add(JlRobot);
	}
}
