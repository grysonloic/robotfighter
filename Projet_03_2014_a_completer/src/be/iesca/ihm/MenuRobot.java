package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import be.iesca.domaine.Bundle;

public class MenuRobot extends JDialog {

	private static final long serialVersionUID = 1L;
	private JLabel JlTitre = new JLabel("Robots", JLabel.CENTER);
	private JButton JbNewRobot = new JButton("Nouveau");
	private JButton JbVidRobot = new JButton("Vider");
	private JButton JbSupRobot = new JButton("Supprimer");
	private JButton JbAnnuler = new JButton("Annuler");
	private JDialog fc ;
	private Bundle bundle = new Bundle();

	public MenuRobot(VueControleurPrincipal vcp) {
		
		this.setModal(true);
		this.bundle=vcp.getBundle();
		this.setPreferredSize(new Dimension(400,300));
		
		// bulles + gestion clavier boutons
		this.JbNewRobot.setToolTipText("Ajouter votre propore robot (ALT+N");
		this.JbNewRobot.setMnemonic(KeyEvent.VK_N);
		
		this.JbVidRobot.setToolTipText("Supprimer tous vos robots (ALT+V");
		this.JbVidRobot.setMnemonic(KeyEvent.VK_V);
		
		this.JbSupRobot.setToolTipText("Supprimer la selection de robots ALT+S");
		this.JbSupRobot.setMnemonic(KeyEvent.VK_S);
		
		this.JbAnnuler.setToolTipText("Retour au menu principal");
		
		
		this.setLayout(new BorderLayout());
		this.add(JlTitre, BorderLayout.NORTH);

		this.add(new ListerRobot(), BorderLayout.CENTER);
		
		this.add(creerPanelBouton(), BorderLayout.SOUTH);
		this.ajouterListenerBoutons();
		
		this.pack();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(vcp);
		this.setVisible(true);
	}

	private JPanel creerPanelBouton() {
		JPanel JpBoutons = new JPanel();
		JpBoutons.add(this.JbNewRobot);
		JpBoutons.add(this.JbVidRobot);
		JpBoutons.add(this.JbSupRobot);
		JpBoutons.add(this.JbAnnuler);

		return JpBoutons;
	}
	
	private void annuler() {
		this.setModal(false);
		this.setVisible(false);
		
	}
	
	

	private void ajouterListenerBoutons() {
		this.JbNewRobot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new NouveauRobot(fc);
			}
		});

		this.JbVidRobot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// A completer
			}
		});

		this.JbSupRobot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// A completer
			}
		});

		this.JbAnnuler.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				annuler();
			}

		});
	}

}
