package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;

public class NouveauRobot extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// declaration des variables de la fenetre
	private JLabel jlTitre = new JLabel("Nouveau Robot",JLabel.CENTER);
	private JLabel jlTexte = new JLabel("Veuillez selectionner votre fichier: ");
	
	private JTextField jtfRobot = new JTextField(15);
	
	private JButton jbParcourir = new JButton("Parcourir");
	private JButton jbOk = new JButton("Accepter");
	private JButton jbAnnuler = new JButton("Annuler");
	
	private Bundle bundle= new Bundle();
	private GestionnaireUseCases gestionnaire =  GestionnaireUseCases.getInstance();
	
	public NouveauRobot(JDialog fc) throws HeadlessException {
		super(fc,"Nouveau Robot");
		this.setModal(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.getRootPane().setDefaultButton(jbOk);
		this.jbAnnuler.setToolTipText("Retour au menu robots");
		this.jbParcourir.setToolTipText("Parcourir les fichiers de l'ordinateur");
		this.jbParcourir.setMnemonic(KeyEvent.VK_P);
		
		//activation listener
		jbParcourir.addActionListener(this);
		jbOk.addActionListener(this);
		jbAnnuler.addActionListener(this);
		
		//ajout des composants
		
		jlTitre.setFont(new Font("Arial", Font.BOLD, 18));
		this.add(jlTitre, BorderLayout.NORTH);
		
		
		JPanel jpCentre = new JPanel();
		jpCentre.add(jlTexte);
		jpCentre.add(jtfRobot);
		jpCentre.add(jbParcourir);
		this.add(jpCentre);
		
		
		JPanel jpSud = new JPanel();
		jpSud.add(jbOk);
		jpSud.add(jbAnnuler);
		this.add(jpSud, BorderLayout.SOUTH);
		
		
		
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void choixFichier(){
		JFileChooser file = new JFileChooser("./src");
		FileFilter filter = new FileNameExtensionFilter("Java Files", "java");
		file.addChoosableFileFilter(filter);
		file.setFileFilter(filter);
		file.showOpenDialog(this);
		file.setVisible(true);
		
		File fich = file.getSelectedFile();
		try{
		jtfRobot.setText(fich.getName());
		}catch(NullPointerException e){}
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == jbAnnuler){
			this.dispose();
		}
		if(e.getSource() == jbOk){
			ajoutRobot();
			
		}
		if(e.getSource() == jbParcourir ){
			choixFichier();
			
		}
	}

	protected void ajoutRobot(){
		
	}

}
