package be.iesca.dao;

import java.util.List;

import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;

public interface RobotDao extends Dao{
	
	boolean ajouterRobot(ClasseRobot robot, User user);
	boolean supprimerRobot (User user, ClasseRobot Robot);
	List<ClasseRobot> listerRobot(User user);
	boolean vider(User user);
}
