// modifi� par Demoulin S�bastien 
package be.iesca.dao;

import be.iesca.domaine.User;

public interface UserDao extends Dao {
	User getUser(String email, String password,String pseudo);

	Boolean inscrireUser(User user);

	Boolean modifierUser(User user);
}
