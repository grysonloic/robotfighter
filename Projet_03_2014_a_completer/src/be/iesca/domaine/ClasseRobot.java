package be.iesca.domaine;

import java.awt.Robot;
import java.io.File;

import be.iesca.util.Compilateur;
import be.iesca.util.Fichier;

public class ClasseRobot {
	public static final String PACKAGE_ROBOTS = "robots";
	private String nom; // nom simple de la classe. Ex: "Fire" (de
						// "robots.Fire.java")

	public ClasseRobot() {
		super();
	}

	@SuppressWarnings("static-access")
	public ClasseRobot(String nom, String path) throws Exception {
		super();
		this.nom = nom;
		File fichier = new File("path" + "nom");
		Fichier deplacement;
		String[] librairies = { "./libs/robocode.jar" };
		Compilateur compil = new Compilateur("./robots/robots", librairies,
				"./robots/robots", true);
		// v�rification de la pr�sence du fichier

		if (fichier.exists()) {
			
			deplacement = new Fichier();
			deplacement.copierFichier(nom, path, "./robots/robots");
			// v�rification compilation

			compil.compiler();

			if (compil.compilationReussie()) {
				
				// Chargement de la classe
				// � placer apr�s la compilation (sinon ClassNotFoundException)
				
				Class<?>  cl= Class.forName(PACKAGE_ROBOTS+"."+nom);
				cl.asSubclass(Robot.class);
				
				if(cl.getClass()== Class.forName("Robot")){
					
				}

			}

		}

		

		// Si la classe n'h�rite pas de Robot, il doit lancer une
		// ClassCastException

		// v�rification du package
	}

	public ClasseRobot(ClasseRobot robot) {
		super();
		this.nom = robot.nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Robot [nom=" + nom + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClasseRobot other = (ClasseRobot) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
}
