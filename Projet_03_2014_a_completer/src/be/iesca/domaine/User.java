package be.iesca.domaine;

public class User {
	private String email; // identifiant unique
	private String nom;
	private String password;
	
	public User() {
		super();
		this.email = "";
		this.nom= "";
		this.password = "";
	}
	
	//Demoulin Sébastien constructeur avec paramétre
	public User(String email, String nom, String password) {
		super();
		this.email = email;
		this.nom = nom;
		this.password = password;
	}
	//Demoulin Sébastien constructeur avec un objet 
	public User(User user) {
		super();
		this.email = user.email;
		this.nom=user.nom;
		this.password = user.password;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", nom=" + nom + ", motDePasse="
				+ password + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

}
